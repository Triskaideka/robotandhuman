# Robot & Human: A Webcomic

[Robot & Human](https://robotandhuman.neocities.org/) is a JavaScript-powered webcomic.  The strips are stored in a JSON file, and jQuery and CSS are used to display the strips on the page.  PNGs are used for the characters.

The point of this exercise was to make a webcomic that could work on [NeoCities](https://neocities.org/), a web host with a small storage limit and no support for server-side scripts.  (This was back around the time of the site's initial launch, when the storage limit was 10 MB.  Last time I checked, free accounts now get 1 GB of storage, which is plenty of space to fit a fair amount of full-sized image files.  If you're looking to start a webcomic without spending much money, and you know a little bit about HTML, Neocities is a reasonable choice!)

If you like webcomic tools and/or comics about robots and humans, then you should also check out another project of mine, the [Poor Man's Comic Publisher](https://gitlab.com/Triskaideka/comicpub).


## Dependencies

The comics are displayed using [jQuery](https://jquery.com/) and two jQuery plugins, [jCanvas](https://plugins.jquery.com/jcanvas/) (to draw the dialogue tags) and [ScrollTo](https://github.com/flesler/jquery.scrollTo) (for animated scrolling).  These files are all included in this repository.  You could probably use different versions of them if you wanted to.

[Hyphenator.js](https://github.com/mnater/hyphenator) is optional; you could use a different build, or not use it at all.  But if your speakers tend to use long words, you'll probably at least want to hyphenate those words manually.

A [Ruby](https://www.ruby-lang.org/) script is run to generate the Atom feed and minify the CSS and JSON files.  It requires the [gems](https://rubygems.org/) [CSSminify](https://rubygems.org/gems/cssminify/), [json-minify](https://rubygems.org/gems/json-minify/), [json_pure](https://rubygems.org/gems/json_pure/), and [tinyatom](https://rubygems.org/gems/tinyatom/).  The gems are of course not included in this repository.


## Writing a new comic

Steps for writing and uploading a new comic are as follows:

1.  Open [comics.json](comics.json) and write the JSON code for the comic by hand.  (Start by copying from [templates.html](templates.html); don't actually write *all* that code by hand.)
2.  Run [prepare.rb](prepare.rb).
3.  Check the new comic in a web browser to make sure it looks okay.
4.  Commit the changes to the git repository, then push them to the git remote(s).
5.  Upload the files that changed (as reported by prepare.rb) to the web host.  Don't forget to upload atom.txt, even though it's in the .gitignore list.  (The [Neocities command-line interface ](https://neocities.org/cli) is handy for this.  Use a command such as `neocities upload comics.json comics.min.json atom.txt`.)
6.  Check the live site in a web browser.
